$(document).ready(function(){
   var disease =[];
   var diseaseSubQuestion=[];
   var diseaseSubSubQuestion=[];
   var bloodThinners=[];
   var PMHAWBT=[];//Past Medical History associated with Blood Thinners
   // var PMHAWBTSubQuestion=[];
   var CardiacMedications=[];

   //Past Medical History methods call
   checkDiabetes();
   // checkCHF();
   checkDiuretics();
   chestPain();
   CheckAsthma();
   Checkallergies();
   checkckd();
   checkArrhythmia();
   checkPacemaker();
   checkBleeding();
   checkBpmeds();
   checkID();
   checkLiver_disease();
   checkAnesthesia_problem();
   checkTobacco();
   checkAlcohol();
   checkPregnancy();
   checkBmi();
   checkPreop_EKG();
   checkPhys_exam();
   checkseizures();

console.log("Question level 1");
   console.log(disease);
   console.log("Question level 2");
   console.log(diseaseSubQuestion);
   console.log("Question level 3");
   console.log(diseaseSubSubQuestion);   
   //Past medical history end
   // Blood thineers
   checkBloodThinners();
   console.log("Blood Thinners");
   console.log(bloodThinners);
   

   // Past Medical History associated with Blood Thinners
   console.log("Past Medical History associated with Blood Thinners");
   checkAFib();
   checkHeart_attack();
   checkStent_heart();
   checkPulmonary_embolism();
   checkDvt();
   checkHeart_valve();
   checkStroke();
   checkTia();
   checkClotting_disorder();
   checkLeg_stent();
   checkCarotid_stent();
   checkVasc_bypass();
   checkRecent_ortho();
   checkPrevention_blood_thinner()
   console.log(PMHAWBT);
   // console.log(PMHAWBTSubQuestion);

   // Cardiac Medications
   console.log("Cardiac Medications");
   checkCardiacMedications()
   console.log(CardiacMedications);
 

printPastMedicalHistory();




  //past medical history methods
  function printPastMedicalHistory(){
   var pmh="";
   for (var i = 0; i < PMHAWBT.length; i++) {
         pmh+=Object.keys(PMHAWBT[i]);
         value= Object.values(PMHAWBT[i]);
         if(value!="yes"){
            for (let key in value)
               for (let key2 in value[key]){
                  if(key2=="time")
                     pmh+=" "+value[key][key2]
               }
               // if(value.hasOwnProperty(key))

                 // console.log(key2," ",value[key][key2]);
               // 
            

            // console.log(Object.values(value));
            
         }
         pmh+=", ";
   }

   $("#pastMedicalHistory").text(pmh);


  }
   //CHF have some problem
   function checkCHF(){
      var chf= getSplitDate("chf");
      alert(chf);
   }
   function checkPhys_exam(){
      var phys_exam=getSplitDate("phys_exam");
      phys_exam=getlastElement(phys_exam);
      if(phys_exam=="yes")
         disease.push("phys exam");
      return;
   }
   function checkPreop_EKG(){
      var preop_EKG=getSplitDate("preop_EKG");
      preop_EKG=getlastElement(preop_EKG);
      if(preop_EKG=="yes"){
         disease.push("Preop EKG");
         var nsr=getSplitDate("nsr");
         nsr=getlastElement(nsr);
         if(nsr=="yes"){
            var ekg_changed=getSplitDate("ekg_changed");
            ekg_changed=getlastElement(ekg_changed);
            diseaseSubSubQuestion.push({"NSR":{"ekg changed":ekg_changed}});
         }
         var ste_std=getSplitDate("ste_std");
         ste_std=getlastElement(ste_std);
         var bigeminy=getSplitDate("bigeminy");
         bigeminy=getlastElement(bigeminy);
         var bundle_block=getSplitDate("bundle_block");
         bundle_block=getlastElement(bundle_block);
         var afib_on_ekg=getSplitDate("afib_on_ekg");
         afib_on_ekg=getlastElement(afib_on_ekg);
         if(afib_on_ekg=="yes"){
            var hr=getSplitDate("hr100");
            hr=getlastElement(hr);
            diseaseSubSubQuestion.push({"afib on ekg":{"HR<100":hr}});
         }
         
         var ekgArray={"NSR":nsr,"ste std":ste_std,"Bigeminy":bigeminy,"bundle block":bundle_block,"afib on ekg":afib_on_ekg};
         diseaseSubQuestion.push({"Preop EKG":ekgArray});
      }
      return;
   }
   function checkBmi(){
      var bmi=getSplitDate("bmi");
      bmi=getlastElement(bmi);
      disease.push("BMI");
      diseaseSubQuestion.push({"BMI":bmi});
      return;
   }
   function checkPregnancy(){
      var pregnancy=getSplitDate("pregnancy");
      pregnancy=getlastElement(pregnancy);
      if(pregnancy=="yes"){
         disease.push("Pregnancy");
      }
      return;
   }
   function checkAlcohol(){
      var alcohol=getSplitDate("alcohol");
      alcohol=getlastElement(alcohol);
      if(alcohol=="yes"){
         disease.push("Alcohol");
      }
      return;
   }
   function checkTobacco(){
      var tobacco=getSplitDate("tobacco");
      tobacco=getlastElement(tobacco);
      if(tobacco=="yes"){
         disease.push("Tobacco");
         return;
      }
   }
   function checkAnesthesia_problem(){
      var anesthesia_problem=getSplitDate("anesthesia_problem");
      anesthesia_problem=getlastElement(anesthesia_problem);
      if(anesthesia_problem=="yes"){
         disease.push("Anesthesia problem");
         return;
      }
   }
   function checkLiver_disease(){
      var liver_disease=getSplitDate("liver_disease");
      liver_disease=getlastElement(liver_disease);
      if(liver_disease=="yes"){
         disease.push("Liver disease");
         return;
      }
   }
   function checkID(){
       var ID=getSplitDate("ID");
         ID=getlastElement(ID);
         if(ID=="no")
            return;
         else if(ID=="yes"){
            disease.push("ID");
            var hiv=getSplitDate("hiv");
            hiv=getlastElement(hiv);
            var hcv=getSplitDate("hcv");
            hcv=getlastElement(hcv);
            var hbv=getSplitDate("hbv");
            hbv=getlastElement(hbv);
            var other_ID=getSplitDate("other_ID");
            other_ID=getlastElement(other_ID);
             var IDArray={ "hiv":hiv,"hcv":hcv,"hbv":hbv,"other ID":other_ID};
             diseaseSubQuestion.push({"ID":IDArray});
         }     
   }
   function checkBpmeds(){
      var bpmeds=getSplitDate("bpmeds");
      bpmeds=getlastElement(bpmeds);
      if(bpmeds=="no")
         return;
      else if(bpmeds=="yes"){
         disease.push("bp meds");
         var bp150=getSplitDate("bp150");
         bp150=getlastElement(bp150);
         var bpmedsArray={"bp150":bp150 };
         diseaseSubQuestion.push({"bp meds":bpmedsArray});
         return;

      }
   }
   function checkBleeding(){
      var Bleeding=getSplitDate("Bleeding");
      Bleeding=getlastElement(Bleeding);
      if(Bleeding=="no")
         return;
      else if(Bleeding=="yes"){
         disease.push("Bleeding");
         var hemophilia =getSplitDate("hemophilia");
         hemophilia=getlastElement(hemophilia);
         var thrombocytopenia=getSplitDate("thrombocytopenia");
         thrombocytopenia=getlastElement(thrombocytopenia);
         var vonwillebrands=getSplitDate("vonwillebrands");
         vonwillebrands=getlastElement(vonwillebrands);
         var other_bleeding=getSplitDate("other_bleeding");
         other_bleeding=getlastElement(other_bleeding);

         var bleedingArray={"Hemophilia":hemophilia,"Thrombocytopenia":thrombocytopenia,"vonwillebrands":vonwillebrands,"other bleeding":other_bleeding};
         diseaseSubQuestion.push({"Bleeding":bleedingArray});
         return;
      }
   }
   function checkPacemaker(){
      var pacemaker=getSplitDate("pacemaker");
      pacemaker=getlastElement(pacemaker);
      if(pacemaker=="no")
         return;
      else if (pacemaker=="yes"){
         disease.push("Pacemaker")
         return;
      }
   }
   function checkArrhythmia(){
      var arrhythmia=getSplitDate("arrhythmia");
      arrhythmia=getlastElement(arrhythmia);
      
      if(arrhythmia=="no")
         return;
      else if (arrhythmia=="yes"){
         disease.push("Arrhythmia");

         var aicd2=getSplitDate("aicd2");
         aicd2=getlastElement(aicd2);
         var arrhythmiaArray={"Aicd2":aicd2};
         diseaseSubQuestion.push({"Arrhythmia":arrhythmiaArray});
         return;
      }
   }
   function checkckd(){
      var ckd =getSplitDate("ckd");
      ckd=getlastElement(ckd);
      
      if(ckd=="no")
         return;
      else if(ckd=="yes"){
         disease.push("CKD");

         var HD=getSplitDate("HD");
         HD=getlastElement(HD);

         var cr2=getSplitDate("cr2");
         cr2=getlastElement(cr2);

         var potassium=getSplitDate("potassium");
         potassium=getlastElement(potassium);

         var ckdArray={"HD":HD, "CR2":cr2,"Potassium":potassium};
         diseaseSubQuestion.push({"CKD":ckdArray});
      }
   }
   function Checkallergies(){
      var allergies=getSplitDate("allergies");
      allergies=getlastElement(allergies);
      
      if(allergies=="no")
         return;
      else if(allergies=="yes"){
         disease.push("Allergies");
          var eggs=getSplitDate("eggs");
          eggs=getlastElement(eggs);
          var soy=getSplitDate("soy");
          soy=getlastElement(soy);
          var pcn =getSplitDate("pcn");
          pcn=getlastElement(pcn);
          var lidocaine=getSplitDate("lidocaine");
          lidocaine=getlastElement(lidocaine);
          var other_allergy=getSplitDate("other_allergy");
          other_allergy=getlastElement(other_allergy);
          var allergiesArray={"Eggs":eggs, "Soy":soy, "Pcn":pcn, "Lidocaine":lidocaine, "Other allergy":other_allergy};
          diseaseSubQuestion.push({"Allergies":allergiesArray});
      }
   }
   function CheckAsthma(){
      var asthma = getSplitDate("asthma_copd");
      asthma=getlastElement(asthma);

      if(asthma=="no")
         return;
      else if(asthma=="yes"){
         disease.push("asthma COPD");
         var wheezing = getSplitDate("wheezing");
         wheezing=getlastElement(wheezing);
         var astamaArray={"Wheezing":wheezing};
         diseaseSubQuestion.push({"asthma COPD":astamaArray});
         return;
      }
   }
   function chestPain(){
      var chestpain=getSplitDate("chestpain");
      chestpain=getlastElement(chestpain);
      if(chestpain=="no")
         return;
      else if(chestpain=="yes"){
         disease.push("Chest Pain");
         var cardsymptoms_cleared=getSplitDate("cardsymptoms_cleared");
         cardsymptoms_cleared=getlastElement(cardsymptoms_cleared);
         var chestpainArray={"cardsymptoms_cleared":cardsymptoms_cleared};
         diseaseSubQuestion.push({"Chest Pain":chestpainArray});
         return;
      }
   }
   function checkDiuretics(){
      var diuretics = getSplitDate("diuretics");
      diuretics=getlastElement(diuretics);
      if(diuretics=="no")
         return;
      else if(diuretics=="yes"){
         disease.push("Diuretics");

         var lb=getSplitDate("5lb");
         lb=getlastElement(lb);
         
         var orthopnea = getSplitDate("orthopnea");
         orthopnea=getlastElement(orthopnea);

         var diureticsArray= {"5lb":lb , "orthopnea":orthopnea};
         diseaseSubQuestion.push({"Diuretics":diureticsArray});
         return;
      }
   }
   function checkDiabetes(){
   	var diabetes = getSplitDate("diabetes");
   	diabetes=getlastElement(diabetes);
   	if(diabetes=="no")
   		return;
   	else if(diabetes=="yes"){
         // If there is Diabetes 
   		disease.push("Diabetes");
   		// sub questions
   		var typediabetes= getSplitDate("typediabetes");
   		typediabetes= getlastElement(typediabetes);

   		var hga1c= getSplitDate("hga1c");
   		hga1c=getlastElement(hga1c);
   		
   		var longactinginsulin = getSplitDate("longactinginsulin");
   		longactinginsulin=getlastElement(longactinginsulin);
   		
   		var sulfonylurea=getSplitDate("sulfonylurea");
   		sulfonylurea=getlastElement(sulfonylurea);
   		
   		var short_insulin=getSplitDate("short_insulin");
   		short_insulin=getlastElement(short_insulin);

   		var diabetesArray={"Type":typediabetes , "hga1c":hga1c , "longactinginsulin": longactinginsulin , "sulfonylurea" : sulfonylurea , "short_insulin": short_insulin};
   		diseaseSubQuestion.push({"Diabetes":diabetesArray});
   		return;
   	}
   }
   function checkseizures(){
      var seizures=getSplitDate("seizures");
      seizures=getlastElement(seizures);
      if(seizures=="yes"){
         disease.push("Seizures");
         var seizures_controlled=getSplitDate("seizures_controlled");
         seizures_controlled=seizures_controlled[seizures_controlled.length-1].replace("_"," ");
         var seizuresArray={"Seizures controlled":seizures_controlled};
         diseaseSubQuestion.push({"Seizures":seizuresArray});
         return;
      }
   }

   // Blood thinners methods
   function checkBloodThinners(){
      // aspirin1
      var aspirin1 =getSplitDate("aspirin1");
      aspirin1=getlastElement(aspirin1);
      if(aspirin1=="yes")
         bloodThinners.push("Aspirin1");
      // plavix1
      var plavix1=getSplitDate("plavix1");
      plavix1=getlastElement(plavix1);
      if(plavix1=="yes")
         bloodThinners.push("Plavix1");
      // effient1
      var effient1=getSplitDate("effient1");
      effient1=getlastElement(effient1);
      if(effient1=="yes")
         bloodThinners.push("Effient1");
      // brillinta1
      var brillinta1=getSplitDate(brillinta1);
      brillinta1=getlastElement(brillinta1);
      if(brillinta1=="yes")
         bloodThinners.push("Brillinta1");
      // agrenox1
      var agrenox1=getSplitDate("agrenox1");
      agrenox1=getlastElement(agrenox1);
      if(agrenox1=="yes")
         bloodThinners.push("Agrenox1");
      // coumadin1
      var coumadin1=getSplitDate("coumadin1");
      coumadin1=getlastElement(coumadin1);
      if(coumadin1=="yes")
         bloodThinners.push("Coumadin1");
      // xarelto1
      var xarelto1=getSplitDate("xarelto1");
      xarelto1=getlastElement(xarelto1);
      if(xarelto1=="yes")
         bloodThinners.push("Xarelto1");
      // eliquis1
      var eliquis1=getSplitDate("eliquis1");
      eliquis1=getlastElement(eliquis1);
      if(eliquis1=="yes")
         bloodThinners.push("Eliquis1");
      // pradaxa1
      var pradaxa1=getSplitDate("pradaxa1");
      pradaxa1=getlastElement(pradaxa1);
      if(pradaxa1=="yes")
         bloodThinners.push("Pradaxa1");
      // lovenox1
      var lovenox1=getSplitDate("lovenox1");
      lovenox1=getlastElement(lovenox1);
      if(lovenox1=="yes")
         bloodThinners.push("Lovenox1");
      // other_bloodthinner1
      var other_bloodthinner1=getSplitDate("other_bloodthinner1");
      other_bloodthinner1=getlastElement(other_bloodthinner1);
      if(other_bloodthinner1=="yes")
         bloodThinners.push("other bloodthinner");

   }

   //Past Medical History associated with Blood Thinners
   function checkAFib(){
      var afib=getSplitDate("afib");
      afib=getlastElement(afib);
      if(afib=="yes"){
         PMHAWBT.push({"Atrial Fibrillation":"yes"});
      }
   }
   function checkHeart_attack(){
      var heart_attack=getSplitDate("heart_attack");
      heart_attack=getlastElement(heart_attack);
      if(heart_attack=="yes"){
         // PMHAWBT.push("Heart attack");
         var time_since_heart_attack=getSplitDate("time_since_heart_attack");
         var time_since_heart_attack=getlastElement(time_since_heart_attack);
         PMHAWBT.push({"Heart attack":{"time":abbreviationToWords(time_since_heart_attack)}});
      }
   }
   function checkStent_heart(){
      var stent_heart=getSplitDate("stent_heart");
      stent_heart=getlastElement(stent_heart);
      if(stent_heart=="yes"){
         // PMHAWBT.push("Stent Heart");
         var time_since_heart_stent=getSplitDate("time_since_heart_stent");
         time_since_heart_stent=getlastElement(time_since_heart_stent);
         PMHAWBT.push({"Stent Heart":{"time":abbreviationToWords(time_since_heart_stent)}});
      }
   }
   function checkPulmonary_embolism(){
      var pulmonary_embolism=getSplitDate("pulmonary_embolism");
      pulmonary_embolism=getlastElement(pulmonary_embolism);
      if(pulmonary_embolism=="yes"){
         // PMHAWBT.push("Pulmonary Embolism");
         var time_since_pulmonary_embolism=getSplitDate("time_since_pulmonary_embolism");
         time_since_pulmonary_embolism=getlastElement(time_since_pulmonary_embolism);
         PMHAWBT.push({"Pulmonary Embolism":{"time":abbreviationToWords(time_since_pulmonary_embolism)}});  
      }
   }
   function checkDvt(){
      var dvt=getSplitDate("dvt");
      dvt=getlastElement(dvt);
      if(dvt=="yes"){
         // PMHAWBT.push("DVT");
         var time_since_dvt=getSplitDate("time_since_dvt");
         time_since_dvt=getlastElement(time_since_dvt);
         PMHAWBT.push({"DVT":{"time":abbreviationToWords(time_since_dvt)}});
      }
   }
   function checkHeart_valve(){
      var heart_valve=getSplitDate("heart_valve");
      heart_valve=getlastElement(heart_valve);
      if(heart_valve=="yes"){
         // PMHAWBT.push("Heart valve");
         var valve_material=getSplitDate("valve_material");
         valve_material=getlastElement(valve_material);
         var time_since_heart_valve=getSplitDate("time_since_heart_valve");
         time_since_heart_valve=getlastElement(time_since_heart_valve);
         heart_valveArray={"Valve material":valve_material,"time":abbreviationToWords(time_since_heart_valve)};
         PMHAWBT.push({"Heart valve":heart_valveArray});
      }
   }
   function checkStroke(){
      var stroke=getSplitDate("stroke");
      stroke=getlastElement(stroke);
      if(stroke=="yes"){
         
         var stroke_type=getSplitDate("stroke_type");
         if(stroke_type[stroke_type.length-1].toLowerCase().split("_").includes("ischemic")){
            var time_since_ischemic_stroke=getSplitDate("time_since_ischemic_stroke");
            time_since_ischemic_stroke=getlastElement(time_since_ischemic_stroke);
            strokeArray={"Type":"Ischemic", "time":abbreviationToWords(time_since_ischemic_stroke)}; 
            
         }
         else if(stroke_type[stroke_type.length-1].toLowerCase().split("_").includes("embolic")){
            var  time_since_embolic_stroke=getSplitDate("time_since_embolic_stroke");
            time_since_embolic_stroke=getlastElement(time_since_embolic_stroke);
            strokeArray={"Type":"Embolic", "time":abbreviationToWords(time_since_embolic_stroke)}; 
            
         }
         PMHAWBT.push({"Stroke":strokeArray});
         
      }
   }
   function checkTia(){
      var tia=getSplitDate("tia");
      tia=getlastElement(tia);
      if(tia=="yes"){
         var time_since_tia=getSplitDate("time_since_tia");
         time_since_tia=getlastElement(time_since_tia);
         PMHAWBT.push({"Tia":{"time":abbreviationToWords(time_since_tia)}});
      }
   }
   function checkClotting_disorder(){
      var clotting_disorder=getSplitDate("clotting_disorder");
      clotting_disorder=getlastElement(clotting_disorder);
      if(clotting_disorder=="yes"){
         var factorV=getSplitDate("factorV");
         factorV=getlastElement(factorV);
         var protein_C_S=getSplitDate("protein_C_S");
         protein_C_S=getlastElement(protein_C_S);
         var lupus_ac=getSplitDate("lupus_ac");
         lupus_ac=getlastElement(lupus_ac);
         var sickle=getSplitDate("sickle");
         sickle=getlastElement(sickle);
         var sickle_trait=getSplitDate("sickle_trait");
         sickle_trait=getlastElement(sickle_trait);
         var other_clotting=getSplitDate("other_clotting");
         other_clotting=getlastElement(other_clotting);
         clotting_disorderArray={"factorV":factorV,"protein_C_S":protein_C_S, "lupus_ac":lupus_ac,"sickle":sickle,"sickle_trait":sickle_trait,"other_clotting":other_clotting};
         PMHAWBT.push({"Clotting disorder":clotting_disorderArray});
      }
   }
   function checkLeg_stent(){
      var leg_stent=getSplitDate("leg_stent");
      leg_stent=getlastElement(leg_stent);
      if(leg_stent=="yes"){
         var time_since_leg_stent=getSplitDate("time_since_leg_stent");
         time_since_leg_stent=getlastElement(time_since_leg_stent);
         PMHAWBT.push({"Leg Stent":{"time":abbreviationToWords(time_since_leg_stent)}});
      }
   }
   function checkCarotid_stent(){
      var carotid_stent=getSplitDate("carotid_stent");
      carotid_stent=getlastElement(carotid_stent);
      if(carotid_stent=="yes"){
         var time_since_carotid_stent=getSplitDate("time_since_carotid_stent");
         time_since_carotid_stent=getlastElement(time_since_carotid_stent);
         PMHAWBT.push({"Carotid Stent":{"time":abbreviationToWords(time_since_carotid_stent)}});
      }
   }
   function checkVasc_bypass(){
      var vasc_bypass=getSplitDate("vasc_bypass");
      vasc_bypass=getlastElement(vasc_bypass);
      if(vasc_bypass=="yes"){
         var time_since_bypass=getSplitDate("time_since_bypass");
         time_since_bypass=getlastElement(time_since_bypass);
         PMHAWBT.push({"Vascular bypass":{"time":abbreviationToWords(time_since_bypass)}});
      }
   }
   function checkRecent_ortho(){
      var recent_ortho=getSplitDate("recent_ortho");
      recent_ortho=getlastElement(recent_ortho);
      if(recent_ortho=="yes"){
          PMHAWBT.push({"Recent orthopedic":"yes"});
      }
   }
   function checkPrevention_blood_thinner(){
      var prevention_blood_thinner=getSplitDate("prevention_blood_thinner");
      prevention_blood_thinner=getlastElement(prevention_blood_thinner);
      if(prevention_blood_thinner=="yes"){
         PMHAWBT.push({"prevention blood thinner":"yes"});
      }
   }

   // Cardiac medications
   function checkCardiacMedications(){
      var metoprolol=getSplitDate("metoprolol");
      metoprolol=getlastElement(metoprolol);
      if(metoprolol=="yes")
         CardiacMedications.push("Metoprolol");

      var carvedilol=getSplitDate("carvedilol");
      carvedilol=getlastElement(carvedilol);
      if(carvedilol=="yes")
         CardiacMedications.push("Carvedilol");
      
      var nadolol=getSplitDate("nadolol");
      nadolol=getlastElement(nadolol);
      if(nadolol=="yes"){
         CardiacMedications.push("Nadolol");
      }
      
      var propranolol=getSplitDate("propranolol");
      propranolol=getlastElement(propranolol);
      if(propranolol=="yes"){
         CardiacMedications.push("Propranolol");
      }
      
      var diltiazem=getSplitDate("diltiazem");
      diltiazem=getlastElement(diltiazem);
      if(diltiazem=="yes")
         CardiacMedications.push("Diltiazem");
      
      var verapamil=getSplitDate("verapamil");
      verapamil=getlastElement(verapamil);
      if(verapamil=="yes"){
         CardiacMedications.push("Verapamil");
      }
      
      var digoxin=getSplitDate("digoxin");
      digoxin=getlastElement(digoxin);
      if(digoxin=="yes"){
         CardiacMedications.push("Digoxin");
      }
      
      var amiodarone=getSplitDate("amiodarone");
      amiodarone=getlastElement(amiodarone);
      if(amiodarone=="yes"){
         CardiacMedications.push("Amiodarone");
      }
      
      var propafenone=getSplitDate("propafenone");
      propafenone=getlastElement(propafenone);
      if(propafenone=="yes"){
         CardiacMedications.push("Propafenone");
      }
      
      var fleicanide=getSplitDate("fleicanide");
      fleicanide=getlastElement(fleicanide);
      if(fleicanide=="yes"){
         CardiacMedications.push("Fleicanide");
      }
      
      var quinidine=getSplitDate("quinidine");
      quinidine=getlastElement(quinidine);
      if(quinidine=="yes"){
         CardiacMedications.push("Quinidine");
      }

   }
   




   // get the current answer 
   function getlastElement(series){
   	var ele= series[series.length-1];
   	if(ele.includes("_"))
   		return ele.split("_")[0];
   	else
   		return ele;
   }
   // Split the data on " : "  
   function getSplitDate(id){
   		return $("#"+id).text().trim().toLowerCase().split(":");
   }

   function abbreviationToWords(str){
      str=str.trim().toLowerCase();
      if(str=="mtoy")
         return "More than one year";
      else if(str=="oyol")
         return "One year or less";
      else if(str=="lt21d")
         return "Less than 21 days ago";
      else if(str=="mt21d")
         return "More than 21 days ago";
      else if(str=="mt3m")
         return "More than 3 months ago";
      else if(str=="prostheticgraft")
         return "Prosthetic graft";
      else if(str=="veingraft")
         return "Vein graft";
      else if(str=="afib")
         return "Atrial Fibrillation"

   }
});